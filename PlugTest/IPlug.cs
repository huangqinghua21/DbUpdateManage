﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlugTest
{
    public class s
    {
       // ITest test = new TestMain();

        public void Show()
        {
            var test = Factory.GetExamples<ITest>();
            test.Show();
        }

    }
    /// <summary>
    /// 工厂
    /// </summary>
    public class Factory
    {
        public static T GetExamples<T>()
        {

            return default(T);
        }

    }
    public interface ITest
    {
        void Show();
    }
    public class TestMain : ITest, IPlugMain
    {
        public void Show()
        {
            string s = "";
        }
    }

    public class TestPatch : ITest, IPlugPatch
    {
        public void Show()
        {
            string s = "";
        }
    }

    /// <summary>
    /// 插件主程序
    /// </summary>
    public interface IPlugMain
    {
    }
    /// <summary>
    /// 插件
    /// </summary>
    public interface IPlugPatch
    {
    }
    /// <summary>
    /// 插件类型
    /// </summary>
    public enum PlugTypeEnum
    {
        /// <summary>
        /// 主程序
        /// </summary>
        Main = 0,
        /// <summary>
        /// 补丁
        /// </summary>
        Patch,
    }
    /// <summary>
    /// 插件特性
    /// </summary>
    public class PlugAttribute : Attribute
    {
        /// <summary>
        /// 插件类型
        /// </summary>
        public PlugTypeEnum PlugType { get; set; }
        /// <summary>
        /// 对应类的实例
        /// </summary>
        public string Example { get; set; }
        /// <summary>
        /// 方法
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 版本
        /// 
        /// </summary>
        public int Ver { get; set; }
    }
}
