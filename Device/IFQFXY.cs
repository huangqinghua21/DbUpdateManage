﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Device
{
    public enum FQFXYDeviceZTS
    {
        调零 = 0,
        检测 = 1,
        反吹 = 2,
        待机 = 3
    }

    /// <summary>
    /// 分析分析仪数据结构
    /// </summary>
    [Serializable]
    public class FQFXYData
    {
        private float _hc = 0.0f;
        private float _co = 0.0f;
        private float _no = 0.0f;
        private float _co2 = 0.0f;
        private float _o2 = 0.0f;
        private float _λ = 0.0f;
        private float _sd = 0.0f;
        private float _yw = 0.0f;
        private float _hjwd = 0.0f;
        private float _zs = 0.0f;
        private float _qlyl = 0.0f;
        private float _hjyl = 0.0f;
        private float _pef = 0.0f;
        private float _n = 0.0f;
        private float _ns = 0.0f;
        private float _k = 0.0f;
        private float _yqwd = 0.0f;

        public float PEF
        {
            get { return _pef; }
            set { _pef = value; }
        }
        /// <summary>
        /// 烟度N值
        /// </summary>
        public float N
        {
            get { return _n; }
            set { _n = value; }
        }
        /// <summary>
        /// 烟度Ns值
        /// </summary>
        public float Ns
        {
            get { return _ns; }
            set { _ns = value; }
        }
        /// <summary>
        /// 烟度K值
        /// </summary>
        public float K
        {
            get { return _k; }
            set { _k = value; }
        }
        /// <summary>
        /// 烟气温度
        /// </summary>
        public float YQWD
        {
            get { return _yqwd; }
            set { _yqwd = value; }
        }

        /// <summary>
        /// 废气HC值
        /// </summary>
        public float HC
        {
            get { return _hc; }
            set { _hc = value; }
        }

        /// <summary>
        /// 废气CO值
        /// </summary>
        public float CO
        {
            get { return _co; }
            set { _co = value; }
        }

        /// <summary>
        /// 废气NO值
        /// </summary>
        public float NO
        {
            get { return _no; }
            set { _no = value; }
        }

        public float NO2 { get; set; }

        public float NOX { get; set; }

        /// <summary>
        /// 废气CO2值
        /// </summary>
        public float CO2
        {
            get { return _co2; }
            set { _co2 = value; }
        }

        /// <summary>
        /// 废气O2值
        /// </summary>
        public float O2
        {
            get { return _o2; }
            set { _o2 = value; }
        }

        /// <summary>
        /// 废气λ值
        /// </summary>
        public float λ
        {
            get { return _λ; }
            set { _λ = value; }
        }

        /// <summary>
        /// 废气相对湿度值
        /// </summary>
        public float SD
        {
            get { return _sd; }
            set { _sd = value; }
        }

        /// <summary>
        /// 废气油温值
        /// </summary>
        public float YW
        {
            get { return _yw; }
            set { _yw = value; }
        }

        /// <summary>
        /// 废气环境温度值
        /// </summary>
        public float HJWD
        {
            get { return _hjwd; }
            set { _hjwd = value; }
        }

        /// <summary>
        /// 废气转速值
        /// </summary>
        public float ZS
        {
            get { return _zs; }
            set { _zs = value; }
        }

        /// <summary>
        /// 废气气路压力值
        /// </summary>
        public float QLYL
        {
            get { return _qlyl; }
            set { _qlyl = value; }
        }

        /// <summary>
        /// 废气环境压力值
        /// </summary>
        public float HJYL
        {
            get { return _hjyl; }
            set { _hjyl = value; }
        }
    }

    /// <summary>
    /// 泄漏状态
    /// </summary>
    public enum LeakStatus
    {
        检查中 = 0,
        无泄漏 = 1,
        有泄漏 = 2
    }
    public interface IFQFXY
    {
        ///// <summary>
        ///// 获取所有废气分析仪实现类名
        ///// </summary>
        ///// <returns></returns>
        //public static string[] GetAllFQFXYType()
        //{
        //    var data = typeof(IFQFXY).Assembly.GetTypes().Where(s => s.IsSubclassOf(typeof(IFQFXY)));
        //    return data.Select(s => s.Name).ToArray();
        //}

        /// <summary>
        /// 设备状态
        /// </summary>
        FQFXYDeviceZTS State { get; set; } //= FQFXYDeviceZTS.待机;

        ///// <summary>
        ///// 状态信息 可升级为枚举
        ///// </summary>
        //public static string StateMsg = "正在测量";



        /// <summary>
        /// 设备调零时间
        /// </summary>
        int ZeroTime { get; set; }


        /// <summary>
        /// 低流量阀值
        /// </summary>
        int DLLXZ { get; set; }

        ///// <summary>
        ///// 获取废气分析仪操作对象
        ///// </summary>
        ///// <param name="type"></param>
        ///// <returns>IFQFXY</returns>
        //public static IFQFXY GetFQFXY(string type)
        //{
        //    var tmp = typeof(IFQFXY).Assembly.GetTypes();
        //    Type T = tmp.FirstOrDefault(s => s.Name == type && s.IsSubclassOf(typeof(IFQFXY)));
        //    if (T == null)
        //        throw new Exception("废气分析仪型号" + type + "不存在！");
        //    IFQFXY fqfxy = (IFQFXY)Activator.CreateInstance(T);
        //    return fqfxy;
        //}

        /// <summary>
        /// 初始化废弃分析仪
        /// </summary>
        /// <param name="comname">串口名称 如COM1</param>
        /// <param name="comstr">串口字符串 如9600,N,8,1</param>
        /// <returns></returns>
        void InitFQFXY(string comname, string comstr);
        /// <summary>
        /// 通信失败次数
        /// </summary>
        int errorcount { get; set; }
        /// <summary>
        /// 开始测量
        /// </summary>
        /// <returns>bool</returns>
        bool StartDetect { get; set; }

        /// <summary>
        /// 停止测量
        /// </summary>
        /// <returns>bool</returns>
        bool StopDetect { get; set; }

        /// <summary>
        /// 调零
        /// </summary>
        /// <returns>bool</returns>
        bool SetZero { get; set; }

        /// <summary>
        /// 反吹
        /// </summary>
        /// <returns>bool</returns>
        bool BackFlow { get; set; }

        /// <summary>
        /// 停止反吹
        /// </summary>
        /// <returns>bool</returns>
        bool StopBackFlow { get; set; }

        /// <summary>
        /// 获取实时数据
        /// </summary>
        /// <returns>FQFXYData</returns>
        FQFXYData GetData(bool IsReadAssistData = false);

        /// <summary>
        /// 获取环境空气数据
        /// </summary>
        /// <returns>FQFXYData</returns>
        FQFXYData GetDataHJ();

        /// <summary>
        /// 获取背景空气数据
        /// </summary>
        /// <returns>FQFXYData</returns>
        FQFXYData GetDataBJ();

        /// <summary>
        /// 获取辅助数据
        /// </summary>
        /// <returns></returns>
        FQFXYData GetAssistData();

        /// <summary>
        /// 获取废气分析仪状态
        /// </summary>
        /// <returns></returns>
        int GetState();

        /// <summary>
        /// 捡漏
        /// </summary>
        /// <returns></returns>
        bool CheckLeak();



        /// <summary>
        /// 获取泄漏状态
        /// </summary>
        /// <returns></returns>
        LeakStatus GetLeakStatus();

        /// <summary>
        /// 低流量检测 true: 低流量，false:不是低流量
        /// </summary>
        /// <returns></returns>
        bool LowFlow();

        /// <summary>
        /// 通检查气（打开检查气口）
        /// </summary>
        bool OpenJCQ();
        /// <summary>
        /// 关闭检查气
        /// </summary>
        /// <returns></returns>
        bool CloseJCQ();
        /// <summary>
        /// 通校准气(打开标定气口)
        /// </summary>
        bool OpenJZQ();
        /// <summary>
        /// 关闭校准气
        /// </summary>
        /// <returns></returns>
        bool CloseJZQ();
        /// <summary>
        /// 标定
        /// </summary>
        /// <param name="co2"></param>
        /// <param name="co"></param>
        /// <param name="hc"></param>
        /// <param name="no"></param>
        /// <param name="o2"></param>
        bool BD(double co2, double co, double hc, double no, double o2);

        //public virtual void SetBDZ(double co2, double co, double hc, double no, double o2)
        //{ }
        /// <summary>
        /// 标定环境温度
        /// </summary>
        /// <param name="wd"></param>
        /// <returns></returns>
        bool DB_HJWD(double wd);
        /// <summary>
        /// 标定环境湿度
        /// </summary>
        /// <param name="sd"></param>
        /// <returns></returns>
        bool DB_HJSD(double sd);
        /// <summary>
        /// 标定环境大气压
        /// </summary>
        /// <param name="dqy"></param>
        /// <returns></returns>
        bool DB_HJDQY(double dqy);


        /// <summary>
        /// 开始背景空气测定
        /// </summary>
        /// <returns></returns>
        bool StartBJKQDetect();


        /// <summary>
        /// 开始环境空气测定
        /// </summary>
        /// <returns></returns>
        bool StartHJKQDetect();


        /// <summary>
        /// 开始HC残留测定
        /// </summary>
        /// <returns></returns>
        bool StartHCCLDetect();
    }

}
