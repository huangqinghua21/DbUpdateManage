﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Device
{
    /// <summary>
    /// 设备
    /// </summary>
    public interface IDevice
    {
        /// <summary>
        /// 打开
        /// </summary>
        /// <returns></returns>
        bool Open();
        /// <summary>
        /// 关闭
        /// </summary>
        /// <returns></returns>
        bool Close();
        /// <summary>
        /// 开始检测
        /// </summary>
        /// <returns></returns>
        bool Start();
        /// <summary>
        /// 结束检测
        /// </summary>
        /// <returns></returns>
        bool Stop();
    }
    /// <summary>
    /// 转速仪
    /// </summary>
    public interface IDeviceZsy : IDevice
    {
    }
    /// <summary>
    /// 烟度计
    /// </summary>
    public interface IDeviceYdj : IDevice
    {
    }

    /// <summary>
    /// 废气分析仪器
    /// </summary>
    public interface IDeviceFQFXY : IDevice
    {
      //  FQFXY GetData();

        /// <summary>
        /// 停止测量
        /// </summary>
        /// <returns>bool</returns>
        bool StopDetect();

        /// <summary>
        /// 调零
        /// </summary> 
        bool SetZero();

        /// <summary>
        /// 反吹
        /// </summary> 
        bool BackFlow();

        /// <summary>
        /// 停止反吹
        /// </summary> 
        bool StopBackFlow();
        /// <summary>
        /// 捡漏
        /// </summary>
        /// <returns></returns>
        bool CheckLeak();
    }
    /// <summary>
    /// Nox设备
    /// </summary>
    public interface IDeviceNox : IDevice
    {
    }
    /*
    public class FQFXY
    {
        public float PEF { get; set; }
        public int HC { get; set; }
        public float CO { get; set; }
        public float CO2 { get; set; }

        public int NO { get; set; }
        public float O2 { get; set; }

        public float P { get; set; }
        public float GT { get; set; }
        public float DT { get; set; }

        public int REV { get; set; }

        public int NO2 { get; set; }

        public int NOX { get; set; }

        public DateTime Time { get; set; }
    }

    */
}
