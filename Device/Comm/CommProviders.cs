﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Devices.Comm.Model;

namespace Common.Devices.Comm
{
    /// <summary>
    /// 通讯提供者
    /// </summary>
    public class CommProviders
    {
        public static ITransmission GetCommunication(string linkString)
        {
            Uri uri = null;
            try
            {
                uri = new UriBuilder(linkString).Uri;
            }
            catch (Exception)
            {
                // ignored
            }

            if (uri == null)
            {
                uri = new SerialPortInfo().ToUri();
            }
            ITransmission result = null;
            switch (uri.Scheme.ToLower())
            {
                case "com":
                    result = new SerialPortHelper(new SerialPortInfo(uri));
                    break;
                case "tcp":
                    result = new TcpClientHelper(new TcpClientInfo(uri));
                    break;
                case "tcpserver":
                    result = new TcpServerHelper(new TcpServerInfo(uri));
                    break;
                default:
                    result = new SerialPortHelper(new SerialPortInfo());
                    break;

            }
            return result;
        }

        public static Dictionary<string, ITransmission> GetCommunication()
        {
            Dictionary<string, ITransmission> models = new Dictionary<string, ITransmission>
            {
                {"com", new SerialPortHelper()},
                {"tcpClient", new TcpClientHelper()},
                {"TcpServer", new TcpServerHelper()}

            };
            return models;

        }
    }
}
