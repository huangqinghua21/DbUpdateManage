﻿using System;
using System.Collections.Generic;

namespace Common.Devices.Comm.Model.Helper
{
    /// <summary>
    /// 属性反射
    /// </summary>
    public class PropertyToString
    {
        public static List<PropertyAttribute> GetProperties<T>(T t)
        {
            var result = new List<PropertyAttribute>();
            //}
            System.Reflection.PropertyInfo[] properties = t.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);


            if (properties.Length <= 0)
            {
                return result;
            }
            Type typeDescription = typeof(PropertyAttribute);
            foreach (System.Reflection.PropertyInfo item in properties)
            {
                string name = item.Name;
                object value = item.GetValue(t, null);
                object[] array = item.GetCustomAttributes(typeDescription, false);
                foreach (Attribute at in array)
                {
                    if (at.GetType() == typeof(PropertyAttribute))
                    {
                        PropertyAttribute info = (PropertyAttribute)at;
                        info.Name = name;
                        if (value != null)
                            info.Value = Convert.ToString(value);
                        result.Add(info);
                    }
                }
            }
            return result;
        }

    }
}
