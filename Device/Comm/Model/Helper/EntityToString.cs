﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Common.Devices.Comm.Model.Helper
{
    public class EntityToString
    {
        /// <summary>
        /// 得到属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns> 
        public static List<EntityString> GetProperties<T>(T t)
        {
            //if (result == null)
            //{
            var result = new List<EntityString>();
            //}
            System.Reflection.PropertyInfo[] properties = t.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);

            Type typeDescription = typeof(DescriptionAttribute);
            if (properties.Length <= 0)
            {
                return result;
            }
            foreach (System.Reflection.PropertyInfo item in properties)
            {
                string name = item.Name;
                object value = item.GetValue(t, null);
                object[] array = item.GetCustomAttributes(typeDescription, false);
                string describe = array.Length > 0 ? ((DescriptionAttribute)array[0]).Description : item.Name;

                if (item.PropertyType.IsValueType || item.PropertyType.Name.StartsWith("String"))
                {

                    result.Add(new EntityString()
                    {
                        Name = name,
                        Value = Convert.ToString(value),
                        Describe = describe,
                        //Type = item.PropertyType.Name
                    });
                }
                //else
                //{
                //    getProperties(value);
                //}
            }
            return result;
        }

    }
}
