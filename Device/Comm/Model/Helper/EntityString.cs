﻿namespace Common.Devices.Comm.Model.Helper
{
    /// <summary>
    /// 实体 string 展示类
    /// </summary>
    public class EntityString
    {
        ///// <summary>
        ///// 值类型
        ///// </summary>
        //public string Type;

        /// <summary>
        /// 值 
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 显示描述
        /// </summary>
        public string Describe { get; set; }
    }

}
