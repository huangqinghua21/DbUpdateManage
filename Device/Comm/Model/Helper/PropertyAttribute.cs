﻿using System;
using System.ComponentModel;

namespace Common.Devices.Comm.Model.Helper
{
    /// <summary>
    /// 属性类型
    /// </summary>
    public enum PropertyType
    {
        [Description("字符串")]
        String = 1,
        [Description("数值")]
        Number,
        [Description("连接")]
        Connect,
        [Description("串口")]
        Serial
    }

    /// <summary>
    /// 属性 的特性
    /// </summary>
    public class PropertyAttribute : Attribute
    {
        public PropertyAttribute(PropertyType type, string describe, string value, string[] options) : this(type, describe, value)
        {
            Options = options;
        }
        public PropertyAttribute(PropertyType type, string describe, string value)
        {
            Type = type;
            Value = value ?? "";
            Describe = describe;
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 类型
        /// </summary>

        public PropertyType Type { get; set; }
        /// <summary>
        /// 选择项
        /// </summary>
        public string[] Options { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string Value { get; set; }

        public string Name { get; set; }
    }




}
