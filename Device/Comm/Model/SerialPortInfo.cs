﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using Common.Devices.Comm.Model.Helper;
using Helper;

namespace Common.Devices.Comm.Model
{

    /// <summary>
    /// 串口参数
    /// </summary>
    public class SerialPortInfo : CommParameter
    {
        public SerialPortInfo()
        {
            PortName = "COM1";
            BaudRate = 9600;
            Parity = Parity.None;
            DataBits = 8;
            StopBits = StopBits.One;
        }

        public SerialPortInfo(Uri uri) : this()
        {
            SetParameter(uri);
        }

        public sealed override void SetParameter(Uri uri)
        {
            PortName = uri.Host;
            BaudRate = int.Parse(uri.GetQueryParameter("BaudRate"));
            SetParity(uri.GetQueryParameter("Parity"));
            DataBits = int.Parse(uri.GetQueryParameter("DataBits"));
            SetStop(uri.GetQueryParameter("StopBits"));
        }

        private void SetStop(string value)
        {
            switch (value)
            {
                case "1":
                case "One":
                    StopBits = StopBits.One;
                    break;
                case "1.5":
                case "3":
                case "OnePointFive":
                    StopBits = StopBits.OnePointFive;
                    break;
                case "2":
                case "Two":
                    StopBits = StopBits.Two;
                    break;
                default:
                    StopBits = StopBits.One;
                    break;
            }
        }

        private void SetParity(string value)
        {
            switch (value)
            {
                case "None":
                    Parity = Parity.None;
                    break;
                case "Odd":
                    Parity = Parity.Odd;
                    break;
                case "Even":
                    Parity = Parity.Even;
                    break;
                case "Mark":
                    Parity = Parity.Mark;
                    break;
                case "Space":
                    Parity = Parity.Space;
                    break;
                default:
                    Parity = Parity.None;
                    break;
            }
        }

        public override Uri ToUri()
        {
            var uriBuilder = new UriBuilder("com://" + PortName + "?" + "BaudRate=" + BaudRate + "&Parity=" + Parity.ToString() + "&Parity=" + Parity + "&DataBits=" + DataBits + "&StopBits=" + (int)StopBits);
            return uriBuilder.Uri;
        }
        public override string ToUriString()
        {
            return ToUri().ToString();

        }

        public override bool SetParameter(List<PropertyAttribute> parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(GetValue(parameter, "PortName")))
                {
                    PortName = GetValue(parameter, "PortName");
                }
                if (!string.IsNullOrEmpty(GetValue(parameter, "BaudRate")))
                {
                    BaudRate = Convert.ToInt32(GetValue(parameter, "BaudRate"));
                }
                if (!string.IsNullOrEmpty(GetValue(parameter, "DataBits")))
                {
                    DataBits = Convert.ToInt32(GetValue(parameter, "DataBits"));
                }

                if (!string.IsNullOrEmpty(GetValue(parameter, "Parity")))
                {
                    SetParity(GetValue(parameter, "Parity"));
                }
                if (!string.IsNullOrEmpty(GetValue(parameter, "StopBits")))
                {
                    SetStop(GetValue(parameter, "StopBits"));
                }
                return true;
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }




        /// <summary>
        /// 端口
        /// </summary>
        [Property(PropertyType.Serial, "端口", "COM1", new[] { "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9" })]
        public string PortName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary> 

        [Property(PropertyType.Number, "波特率", "9600", new[] { "110", "300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "38400", "56000", "57600", "115200", "128000", "256000" })]
        public int BaudRate { get; set; }
        /// <summary>
        /// 校验位
        /// </summary>
        [Property(PropertyType.Number, "校验位", "None", new[] { "None", "Odd", "Even", "Mark", "Space" })]
        public Parity Parity { get; set; }
        /// <summary>
        /// 数据位
        /// </summary>
        [Property(PropertyType.Number, "数据位", "8", new[] { "5", "6", "7", "8" })]
        public int DataBits { get; set; }
        /// <summary>
        /// 停止位
        /// </summary>
        [Property(PropertyType.Number, "停止位", "One", new[] { "One", "Two", "OnePointFive" })]
        public StopBits StopBits { get; set; }
    }

}
