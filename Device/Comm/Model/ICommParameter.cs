﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Devices.Comm.Model.Helper;

namespace Common.Devices.Comm.Model
{
    /// <summary>
    /// 通讯参数
    /// </summary>
    public abstract class CommParameter
    {
        public abstract Uri ToUri();
        public abstract string ToUriString();
        /// <summary>
        /// 设置参数
        /// </summary>
        public abstract bool SetParameter(List<PropertyAttribute> parameter);

        public abstract void SetParameter(Uri uri);
        public void SetParameter(string str)
        {
            try
            {
                var uri = new Uri(str);
                SetParameter(uri);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public virtual string GetValue(List<PropertyAttribute> parameter, string key)
        {
            string result = "";
            var param = parameter.ToDictionary(t => t.Name);
            if (param.ContainsKey(key))
            {
                result = param[key].Value;
            }
            return result;
        }

        public virtual List<PropertyAttribute> GetConfig()
        {
            return PropertyToString.GetProperties(this);
        }
    }
}
