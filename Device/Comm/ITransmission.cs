﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Common.Devices.Comm.Model; 
namespace Common.Devices.Comm
{
    /// <summary>
    /// 通讯传输接口
    /// </summary>
    public interface ITransmission
    {
        /// <summary>
        /// 参数
        /// </summary>
        CommParameter Parameter { get; set; }

        /// <summary>
        /// 接受消息事件
        /// </summary>
        event Action<byte[]> Received;
        /// <summary>
        /// 打开
        /// </summary>
        /// <returns></returns>
        bool Open();
        /// <summary>
        /// 关闭
        /// </summary>
        /// <returns></returns>
        bool Close();
        /// <summary>
        /// 是否打开
        /// </summary>
        /// <returns></returns>
        bool IsOpen();
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        bool Send(byte[] buffer);

   
         

    }




}
