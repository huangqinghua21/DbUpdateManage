﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Detect;

using System.Data.Entity.Migrations;
using System.IO;
using DbUpdateManage;
using DbUpdateManageSql;
using Detect.DetectMethod;
using Device;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string DQ = "QG";
        private void button1_Click(object sender, EventArgs e)
        {
          Adapter.DetecStart("11","123");

            //IS s = new Cq();
            //var temp = s.Get(1, 1, 1);
            ////
            //s = new Xj();
            //temp = s.Get(1, 1, 1);

        }
        /// <summary>
        /// 适配
        /// </summary>
        /// <param name="dq"></param>
        /// <returns></returns>
        public IS GetClass(string dq)
        {
            IS result = null;
            switch (dq)
            {
                case "Xj":
                    result = new Xj();
                    break;
                case "Cq":
                    result = new Cq();
                    break;
            }
            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sys_config config = new sys_config
            {
                COFIG_KEY = "ss",
                Remarks = "Remarks",
                //Remarks2 = "Remarks2"
            };

            //using (var db = new TestEntities())
            //{
            //    db.Database.Log = Log;

            //    var model = db.Set<sys_config>().ToList();
            //    //var ddd = db.sys_config.ToList();
            //    //var sss = db.sys_config2.ToList();
            //    //db.Entry(config).State = EntityState.Added;
            //    //var temp = db.SaveChanges();
            //}



            string s = "";

        }
        public void Log(string log)
        {
            string s = "";

#if DEBUG
            //var logInfo = LogFactory.GetLogger("sql执行记录");
            //logInfo.Info(log);
            //   string dd = log;
#endif
        }
        private void button3_Click(object sender, EventArgs e)
        {
            RepositoryBase repository = new RepositoryBase();
            var models = repository.GetList<sys_config>();
            string s = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DbSynchronizer synchronizer = new DbSynchronizer();
            synchronizer.Register<Tests>();
            synchronizer.Check(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        }
        public class Tests
        { 
            [DbColumn("int", true, IsIdentity = true)]
            public int Id { get; set; }

            [DbColumn("nvarchar", IsPrimaryKey = true, Length = 36)]
            public string Key { get; set; }
            [DbColumn("nvarchar", Length = 255)]
            public string Name { get; set; }
            [DbColumn("nvarchar", Length = 255)]
            public string Remark { get; set; }
            [DbColumn("decimal")]
            public decimal Number { get; set; }

            [DbColumn("float")]
            public float Number2 { get; set; }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            CreateFileContent("c:\\dd.txt", "dd");
        }
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="content">内容</param>
        public static void CreateFileContent(string path, string content)
        {
            FileInfo fi = new FileInfo(path);
            var di = fi.Directory;
            if (!di.Exists)
            {
                di.Create();
            }
            StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.GetEncoding("GB2312"));
            sw.Write(content);
            sw.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
          var  _implementation = DeviceManage.GetImplementationS<IDevice>(System.IO.Directory.GetCurrentDirectory()).ToDictionary(t => t.Text);
            Implementation_ComboBox.Items.AddRange(_implementation.Keys.ToArray());
           // Implementation_ComboBox.SelectedIndex = Implementation_ComboBox.Items.IndexOf(deviceName);
        }
        //public int Insert(TEntity entity)
        //{
        //    Dbcontext.Entry(entity).State = EntityState.Added;
        //    return Dbcontext.SaveChanges();
        //}
    }

    internal class Configuration
    {
    }

    public interface IS
    {
        int Get(int a, int b, int c);
    }

    public class Comm//基础
    {
        /// <summary>
        /// 乘法
        /// </summary>
        /// <param name="a"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int Multiplication(int a, int c)
        {
            //日志
            //

            var result = a + c;
            return result;
        }
        /// <summary>
        /// 除法
        /// </summary>
        /// <param name="a"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int Division(int a, int c)
        {
            var result = a / c;
            return result;
        }

        /// <summary>
        /// 加法
        /// </summary>
        /// <param name="a"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int Addition(int a, int c)
        {
            var result = a + c;
            return result;
        }
        /// <summary>
        /// 减法
        /// </summary>
        /// <param name="a"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int Subtraction(int a, int c)
        {
            var result = a - c;
            return result;
        }


    }

    public class Base : IS
    {

        public string DQ = "QG";
        public Comm Comm = new Comm();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="compute"></param>
        /// <returns></returns>
        public virtual int Get(int a, int b, Func<int, int, int> compute)
        {
            //基础 
            var temp = Comm.Addition(1, 2);
            return compute.Invoke(temp, b);
        }

        public virtual int Get(int a, int b, int c)
        {
            return Get(a, b, Comm.Addition);//j加法
        }
    }

    public class Cq : Base
    {
        public Cq()
        {
            DQ = "Cq";
        }
    }
    public class Xj : Base
    {
        //实现
        //界面实现
        //打印实现
        //检测实现

        //public Xj(界面实现, 打印实现,检测实现)
        //{
        //}

        public Xj()
        {
            DQ = "Xj";
        }

        public override int Get(int a, int b, int c)
        {

            Get(a, b, Comm.Subtraction);//减法
            return Get(a, b, GetInt);//特殊自定义

        }
        private int GetInt(int a, int b)
        {
            var result = (a - b) * (a + b);
            return result;
            ;
        }

    }
}
