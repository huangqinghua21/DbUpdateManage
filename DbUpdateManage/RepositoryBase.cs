﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DbUpdateManage
{
    /// <summary>
    /// 仓储实现
    /// </summary>
    public class RepositoryBase
    {
        public List<TEntity> GetList<TEntity>() where TEntity : class
        {

            NDbContext.GetTypes += Db_GetTypes;
            NDbContext.Register<sys_config2>();
            // NDbContext.Register<sys_config2>();
            // NDbContext.Register<sys_config3>();

            NDbContext db = new NDbContext();
            using (db)
            {
                return db.Set<TEntity>().ToList();
            }
        }

        private List<System.Type> Db_GetTypes()
        {
            List<Type> types = new List<Type>()
            {

                typeof(sys_config3),
                //typeof(Test)
            };
            return types;
            // throw new System.NotImplementedException();
        }
    }
}
