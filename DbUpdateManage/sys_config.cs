using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbUpdateManage
{
    /// <summary>
    /// model接口
    /// </summary>
    public interface IModel
    {
    }

    //public class Test
    //{
    //    [Key]
    //    [Column(Order = 0)]
    //    public int Id { get; set; }
    //    [Key]
    //    [Column(Order = 2)]
    //    [Required]
    //    [StringLength(250)]
    //    public string Key { get; set; }
    //    public  string Name { get; set; }
    //}
    public partial class sys_config4
    {
        [Key]
        public string CATEGORY { get; set; }
        public string ID { get; set; }
    }
    public partial class sys_config3
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        [StringLength(250)]
        public string CATEGORY { get; set; }

    }

    public partial class sys_config2
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        [StringLength(250)]
        public string CATEGORY { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(250)]
        public string COFIG_KEY { get; set; }

        [Column(TypeName = "ntext")]
        public string COFIG_VALUE { get; set; }

        [Column(TypeName = "ntext")]
        public string DESCRIPTION { get; set; }
    }

    //数据 基础 IModel
    //数据库 完善 
    //model
    //调用成
    //[DataContract]
    //public partial class sys_configMap : EntityTypeConfiguration<sys_config>
    //{
    //    public sys_configMap()
    //    {
    //        this.ToTable("sys_config");
    //         this.HasKey(l => l.ID);
    //    }
    //}
    public partial class sys_config : IModel
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }
        [Key]
        [Column(Order = 2)]
        [Required]
        [StringLength(250)]
        public string CATEGORY { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(250)]
        public string COFIG_KEY { get; set; }

        [Column(TypeName = "ntext")]
        public string COFIG_VALUE { get; set; }

        [Column(TypeName = "ntext")]
        public string DESCRIPTION { get; set; }

        [StringLength(250)]
        public string PARAM1 { get; set; }

        [StringLength(250)]
        public string PARAM2 { get; set; }

        [StringLength(250)]
        public string PARAM3 { get; set; }

        [StringLength(250)]
        public string PARAM4 { get; set; }

        [StringLength(250)]
        public string Remarks { get; set; }

        [StringLength(250)]
        public string remarks1 { get; set; }
        [StringLength(250)]
        public string remarks2 { get; set; }
        //  [StringLength(250)]
        public decimal ss { get; set; }
    }
}
