﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbUpdateManage
{
    /// <summary>
    /// VMAS:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class VMAS : IModel
    {

        /// <summary>
        /// 检测编号
        /// </summary>
        [Key]
        [Column(Order = 0)]
        public string JCBH { get; set; }



        private int m_JCCS = 0;
        /// <summary>
        /// 检测次数
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public int JCCS { get; set; }

        private decimal m_TEMPERATURE;
        /// <summary>
        /// 温度
        /// </summary>
        public decimal TEMPERATURE { get; set; }

        private decimal m_HUMIDITY;
        /// <summary>
        /// 湿度
        /// </summary>
        public decimal HUMIDITY { get; set; }

        private decimal m_ATMOSPHERE;
        /// <summary>
        /// 大气压
        /// </summary>
        public decimal ATMOSPHERE { get; set; }

        private string m_PER_SEC_OIL_TEMP = "";
        /// <summary>
        /// 每秒油温
        /// </summary>
        public String PER_SEC_OIL_TEMP { get; set; }

        private string m_PER_SEC_STATUS = "";
        /// <summary>
        /// 每秒检测状态
        /// </summary>
        public String PER_SEC_STATUS { get; set; }

        private string m_PER_SEC_TIME = "";
        /// <summary>
        /// 每秒时间序列
        /// </summary>
        public String PER_SEC_TIME { get; set; }

        private string m_PER_SEC_PLHP = "";
        /// <summary>
        /// 每秒寄生功率
        /// </summary>
        public String PER_SEC_PLHP { get; set; }

        private string m_PER_SEC_INDICATED_POWER = "";
        /// <summary>
        /// 每秒指示功率
        /// </summary>
        public String PER_SEC_INDICATED_POWER { get; set; }

        private string m_PER_SEC_HC = "";
        /// <summary>
        /// 每秒HC测量值
        /// </summary>
        public String PER_SEC_HC { get; set; }

        private string m_PER_SEC_CO = "";
        /// <summary>
        /// 每秒CO测量值
        /// </summary>
        public String PER_SEC_CO { get; set; }

        private string m_PER_SEC_NOx = "";
        /// <summary>
        /// 每秒NOx测量值
        /// </summary>
        public String PER_SEC_NOx
        {
            get { return m_PER_SEC_NOx; }
            set { m_PER_SEC_NOx = value; }
        }
        private string m_PER_SEC_CO2 = "";
        /// <summary>
        /// 每秒CO2测量值
        /// </summary>
        public String PER_SEC_CO2
        {
            get { return m_PER_SEC_CO2; }
            set { m_PER_SEC_CO2 = value; }
        }
        private string m_PER_SEC_O2 = "";
        /// <summary>
        /// 每秒O2测量值
        /// </summary>
        public String PER_SEC_O2
        {
            get { return m_PER_SEC_O2; }
            set { m_PER_SEC_O2 = value; }
        }
        private string m_PER_SEC_λ = "";
        /// <summary>
        /// 每秒λ测量值
        /// </summary>
        public String PER_SEC_λ
        {
            get { return m_PER_SEC_λ; }
            set { m_PER_SEC_λ = value; }
        }
        private string m_PER_SEC_ACT_SPEED = "";
        /// <summary>
        /// 每秒实际车速
        /// </summary>
        public String PER_SEC_ACT_SPEED
        {
            get { return m_PER_SEC_ACT_SPEED; }
            set { m_PER_SEC_ACT_SPEED = value; }
        }
        private string m_PER_SEC_STD_SPEED = "";
        /// <summary>
        /// 每秒目标车速
        /// </summary>
        public String PER_SEC_STD_SPEED
        {
            get { return m_PER_SEC_STD_SPEED; }
            set { m_PER_SEC_STD_SPEED = value; }
        }
        private string m_PER_SEC_REV = "";
        /// <summary>
        /// 每秒转速
        /// </summary>
        public String PER_SEC_REV
        {
            get { return m_PER_SEC_REV; }
            set { m_PER_SEC_REV = value; }
        }
        private string m_PER_SEC_FORCE = "";
        /// <summary>
        /// 每秒扭力
        /// </summary>
        public String PER_SEC_FORCE
        {
            get { return m_PER_SEC_FORCE; }
            set { m_PER_SEC_FORCE = value; }
        }
        private string m_PER_SEC_HC_EV = "";
        /// <summary>
        /// 每秒HC排放值
        /// </summary>
        public String PER_SEC_HC_EV
        {
            get { return m_PER_SEC_HC_EV; }
            set { m_PER_SEC_HC_EV = value; }
        }
        private string m_PER_SEC_CO_EV = "";
        /// <summary>
        /// 每秒CO排放值
        /// </summary>
        public String PER_SEC_CO_EV
        {
            get { return m_PER_SEC_CO_EV; }
            set { m_PER_SEC_CO_EV = value; }
        }
        private string m_PER_SEC_NOx_EV = "";
        /// <summary>
        /// 每秒NOx排放值
        /// </summary>
        public String PER_SEC_NOx_EV
        {
            get { return m_PER_SEC_NOx_EV; }
            set { m_PER_SEC_NOx_EV = value; }
        }
        private string m_PER_SEC_CO2_EV = "";
        /// <summary>
        /// 每秒CO2排放值
        /// </summary>
        public String PER_SEC_CO2_EV
        {
            get { return m_PER_SEC_CO2_EV; }
            set { m_PER_SEC_CO2_EV = value; }
        }
        private string m_PER_SEC_O2_EV = "";
        /// <summary>
        /// 每秒O2排放值
        /// </summary>
        public String PER_SEC_O2_EV
        {
            get { return m_PER_SEC_O2_EV; }
            set { m_PER_SEC_O2_EV = value; }
        }
        private string m_PER_SEC_FL_TEMP = "";
        /// <summary>
        /// 每秒流量计温度
        /// </summary>
        public String PER_SEC_FL_TEMP
        {
            get { return m_PER_SEC_FL_TEMP; }
            set { m_PER_SEC_FL_TEMP = value; }
        }
        private string m_PER_SEC_FL_O2 = "";
        /// <summary>
        /// 每秒流量计O2
        /// </summary>
        public String PER_SEC_FL_O2
        {
            get { return m_PER_SEC_FL_O2; }
            set { m_PER_SEC_FL_O2 = value; }
        }
        private string m_PER_SEC_FL_PRESSURE = "";
        /// <summary>
        /// 每秒流量计压力
        /// </summary>
        public String PER_SEC_FL_PRESSURE
        {
            get { return m_PER_SEC_FL_PRESSURE; }
            set { m_PER_SEC_FL_PRESSURE = value; }
        }
        private string m_PER_SEC_FL_NON_FLOW = "";
        /// <summary>
        /// 每秒流量计非标流量
        /// </summary>
        public String PER_SEC_FL_NON_FLOW
        {
            get { return m_PER_SEC_FL_NON_FLOW; }
            set { m_PER_SEC_FL_NON_FLOW = value; }
        }
        private string m_PER_SEC_FL_STD_FLOW = "";
        /// <summary>
        /// 每秒流量计标准流量
        /// </summary>
        public String PER_SEC_FL_STD_FLOW
        {
            get { return m_PER_SEC_FL_STD_FLOW; }
            set { m_PER_SEC_FL_STD_FLOW = value; }
        }
        private string m_PER_SEC_FL_VEHICLE_FLOW = "";
        /// <summary>
        /// 每秒车辆排气流量
        /// </summary>
        public String PER_SEC_FL_VEHICLE_FLOW
        {
            get { return m_PER_SEC_FL_VEHICLE_FLOW; }
            set { m_PER_SEC_FL_VEHICLE_FLOW = value; }
        }
        private string m_PER_SEC_DILUTION_RATIO = "";
        /// <summary>
        /// 每秒稀释比系数
        /// </summary>
        public String PER_SEC_DILUTION_RATIO
        {
            get { return m_PER_SEC_DILUTION_RATIO; }
            set { m_PER_SEC_DILUTION_RATIO = value; }
        }
        private string m_PER_SEC_HUMIDITY_COEFFICIENT = "";
        /// <summary>
        /// 每秒湿度修正系数
        /// </summary>
        public String PER_SEC_HUMIDITY_COEFFICIENT
        {
            get { return m_PER_SEC_HUMIDITY_COEFFICIENT; }
            set { m_PER_SEC_HUMIDITY_COEFFICIENT = value; }
        }
        private string m_MILEAGE = "";
        /// <summary>
        /// 累计行驶里程
        /// </summary>
        public String MILEAGE
        {
            get { return m_MILEAGE; }
            set { m_MILEAGE = value; }
        }
        private string m_HC_EV = "";
        /// <summary>
        /// HC排放值
        /// </summary>
        public String HC_EV
        {
            get { return m_HC_EV; }
            set { m_HC_EV = value; }
        }
        private string m_CO_EV = "";
        /// <summary>
        /// CO排放值
        /// </summary>
        public String CO_EV
        {
            get { return m_CO_EV; }
            set { m_CO_EV = value; }
        }
        private string m_NOx_EV = "";
        /// <summary>
        /// NOx排放值
        /// </summary>
        public String NOx_EV
        {
            get { return m_NOx_EV; }
            set { m_NOx_EV = value; }
        }
        private string m_HCNOx_EV = "";
        /// <summary>
        /// HCNOx排放值
        /// </summary>
        public String HCNOx_EV
        {
            get { return m_HCNOx_EV; }
            set { m_HCNOx_EV = value; }
        }
        private string m_CO2_EV = "";
        /// <summary>
        /// CO2排放值
        /// </summary>
        public String CO2_EV
        {
            get { return m_CO2_EV; }
            set { m_CO2_EV = value; }
        }
        private string m_HC_LIMIT = "";
        /// <summary>
        /// HC限值
        /// </summary>
        public String HC_LIMIT
        {
            get { return m_HC_LIMIT; }
            set { m_HC_LIMIT = value; }
        }
        private string m_CO_LIMIT = "";
        /// <summary>
        /// CO限值
        /// </summary>
        public String CO_LIMIT
        {
            get { return m_CO_LIMIT; }
            set { m_CO_LIMIT = value; }
        }
        private string m_NOx_LIMIT = "";
        /// <summary>
        /// NOx限值
        /// </summary>
        public String NOx_LIMIT
        {
            get { return m_NOx_LIMIT; }
            set { m_NOx_LIMIT = value; }
        }
        private string m_HCNOx_LIMIT = "";
        /// <summary>
        /// HCNOx限值
        /// </summary>
        public String HCNOx_LIMIT
        {
            get { return m_HCNOx_LIMIT; }
            set { m_HCNOx_LIMIT = value; }
        }
        private string m_HC_JUDGE = "";
        /// <summary>
        /// HC判定
        /// </summary>
        public String HC_JUDGE
        {
            get { return m_HC_JUDGE; }
            set { m_HC_JUDGE = value; }
        }
        private string m_CO_JUDGE = "";
        /// <summary>
        /// CO判定
        /// </summary>
        public String CO_JUDGE
        {
            get { return m_CO_JUDGE; }
            set { m_CO_JUDGE = value; }
        }
        private string m_NOx_JUDGE = "";
        /// <summary>
        /// NOx判定
        /// </summary>
        public String NOx_JUDGE
        {
            get { return m_NOx_JUDGE; }
            set { m_NOx_JUDGE = value; }
        }
        private string m_HCNOx_JUDGE = "";
        /// <summary>
        /// HCNOx判定
        /// </summary>
        public String HCNOx_JUDGE
        {
            get { return m_HCNOx_JUDGE; }
            set { m_HCNOx_JUDGE = value; }
        }
        private string m_JUDGE = "";
        /// <summary>
        /// 总判定
        /// </summary>
        public String JUDGE
        {
            get { return m_JUDGE; }
            set { m_JUDGE = value; }
        }
        private string m_PARAM1 = "";
        public String PARAM1
        {
            get { return m_PARAM1; }
            set { m_PARAM1 = value; }
        }
        private string m_PARAM2 = "";
        public String PARAM2
        {
            get { return m_PARAM2; }
            set { m_PARAM2 = value; }
        }
        private string m_PARAM3 = "";
        public String PARAM3
        {
            get { return m_PARAM3; }
            set { m_PARAM3 = value; }
        }
        private string m_PARAM4 = "";
        public String PARAM4
        {
            get { return m_PARAM4; }
            set { m_PARAM4 = value; }
        }
        public String PARAM5 { get; set; }
    }
}

