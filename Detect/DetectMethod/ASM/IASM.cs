﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Detect.DetectMethod.ASM
{
    /// <summary>
    /// 稳态工况法
    /// </summary>
    public interface IASM
    {
        AmsProcess Process { get; set; }
        /// <summary>
        /// 测量值变化事件
        /// </summary>
        event Action<int> ValueChange;


        ///// <param name="dataAction">数据存储方法</param>
        ///// <param name="lwAction">联网方法</param>
        ///// <param name="xz">限值</param>
        //void BeginDetect(Action dataAction, Action lwAction, int xz);

        /// <summary>
        /// 开始检测
        /// </summary>
        /// <param name="xz"></param>
        void BeginDetect(  int xz);

    }
}
