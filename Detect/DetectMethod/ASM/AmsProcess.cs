﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Detect.DetectMethod.ASM
{ 
    /// <summary>
    /// 稳定工况过程
    /// </summary>
    public class AmsProcess
    {
        public AmsProcess()
        {
            准备 = new DetectProcess();
            稳定车速 =new DetectProcess();
            稳定计时10s = new DetectProcess();
            快速通过5025 = new DetectProcess();
            阶段5025 = new DetectProcess();
            稳定车速40 = new DetectProcess();
            稳定计时 = new DetectProcess();
            快速通过 = new DetectProcess();
            阶段2540 = new DetectProcess();
            结束 = new DetectProcess();
        }
        public DetectProcess 准备 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 稳定车速 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 稳定计时10s { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 快速通过5025 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 阶段5025 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 稳定车速40 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 稳定计时 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 快速通过 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DetectProcess 阶段2540 { get; set; }

        public DetectProcess 结束 { get; set; }
    }
}
