﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Detect.DetectMethod.ASM
{

    public interface IAmsUpdate
    {
        /// <summary>
        /// 过程
        /// </summary>
        AmsProcess Process { get; set; }
    }

    /// <summary>
    /// 稳态上传
    /// </summary>
    public class AmsUpdate : IAmsUpdate
    {
        public AmsProcess Process { get; set; } 
        public AmsUpdate(AmsProcess process)
        {
            Process = process; 
            //需要有结果返回的 不要用多播委托
            Process.快速通过.CentreEvent += ((string s) =>
            {
                //上传数据
                string sp = s;
                return false;
            });
            Process.快速通过.CentreEvent += Fun2;
        }

        public bool Fun2(string s)
        {
            //拍照片
            return true;
        }
    }
}
