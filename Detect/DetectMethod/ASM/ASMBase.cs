﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Detect.DetectMethod.ASM
{
    /// <summary>
    /// 稳态工况 基础
    /// </summary>
    public class ASMBase : IASM
    {
        public ASMBase()
        {
            Process = new AmsProcess();
        }

        /// <summary>
        /// 检测过程
        /// </summary>
        public AmsProcess Process { get; set; }
        /// <summary>
        /// 值变化
        /// </summary>
        public event Action<int> ValueChange;
        /// <summary>
        /// 值变化
        /// </summary>
        /// <param name="data"></param>
        public void OnValueChange(int data)
        {
            ValueChange?.Invoke(data);
        }
        /// <summary>
        /// 开始检测
        /// </summary>

        /// <param name="xz"></param>
        //  public virtual void BeginDetect(Action dataAction, Action lwAction, int xz)
        public virtual void BeginDetect(int xz)
        {
            Process.快速通过.CentreEvent.Invoke("");
            OnValueChange(1);
            //ValueChange.Invoke(1);
        }
    }

    public class ASM_2018A : ASMBase
    {
        public ASM_2018A() : base()
        {
        }

        // public new event Action<int> ValueChange;
        public override void BeginDetect(int xz)
        {
            var s = Process.快速通过.CentreEvent?.Invoke("234");

            Process.快速通过.EndEvent?.Invoke("12");
            OnValueChange(1);
        }
    }

}
