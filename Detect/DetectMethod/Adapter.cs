﻿using System;
using Detect.DetectMethod.ASM;

//1、功能
//检查
//      设备操作
//      检查流程
//联网
//设备自检

//2、业务
//登录、变更检测方法、检测车辆查询；（本地数据、联网平台）
//外检
//尾气检测
//上传平台
//打印报告


//2、环保程序问题 
//基本情况:地区复杂、设备厂家复杂、联网厂家复杂
//1、程序代码复杂 一个方法中多个，为适应不同地区、设备、厂家
//2、维护复杂（不同地区里面的配置文件不一样）
//3、联网与检测代码分开，好处环保检测不考虑联网；比较麻烦上传和本地都存在分支
//4、代码不满足 迪米特法则（又叫最少知识原则，一个软件实体应当尽可能少的与其他实体发生相互作用。）
//为统一使用方法，用数组类型传参
//5、日志写比较乱


//解决方法
//检测方法与联网 采用面向抽象设计不依赖某个实例（现在设备实现）

//1、采用适配器模式，在程序启动时候，根据不同地区、不同联网厂家，为检测方法提供不同的实例。
//2、采用观察者模式（上传观察整个流程的变化）把环保检测过程、上传、拍照分离，降低耦合；把整个按照流程分解（安检上传类似）
//大体分： 外检、发车、检测；
//细分：准备外检、开始检外检、 外检检查中 、外检完成

//新考虑功能
//1、新版本发布，自动更新，每次启动报告平台运行的版本号，平台可以推送升级版本
//2、所有检查站工位程序的运行监管（1运行版本、2异常日志监管上传 、3上传打包文件、4推送拉取文件（配置文件、脚本文件））
//3、检测过程数据是否存档，程序回放。 为排错做支持
//程序监管（考虑）网络、监管是否允许


//登录程序
//1、限值配置
//2、车辆注册信息配置
//3、
//登录
//hw51 注册车辆信息
//hwdl 登录检查员工信息


//过程开始
//检测过程数据
//检测结果
namespace Detect.DetectMethod
{

    /// <summary>
    /// 检测方法创建工厂
    /// </summary>
    public static class Adapter
    {
        //
        //检测方法（不同国标、区域版本2018 ASM_2005） 
        /// <summary>
        /// 检测开始
        /// </summary>
        /// <param name="area">区域</param>
        /// <param name="licensePlate">车牌</param>
        public static void DetecStart(string area, string licensePlate)
        {
            //1、根据车牌查询车辆信息，确定检测方法
            //2、验证是否可以进行工位检测
            //3、根据地区、检测方法获取检测实现

            string DetectMethod = "ASM";
            var realize = GetRealize(area);

            switch (DetectMethod)
            {
                case "ASM":
                    //1、创建过程对象
                    //2、选择对应的上传对象
                    //3、创建Ui对象
                    //4、创建检测方法 执行检测方法
                    AmsUpdate update=new AmsUpdate(realize.Asm.Process);
                    realize.Asm.BeginDetect(0);
                    break;

            }
        }
        /// <summary>
        /// 获区域的实现
        /// </summary>
        /// <param name="area">地区</param> 
        /// <returns></returns>
        public static IAreaRealize GetRealize(string area)
        {
            IAreaRealize result = null;
            switch (area)
            {
                case "JS":
                    break;
                default:
                    result = new Nationwide();//全国
                    break;

            }

            return result;
        }
    }


    /// <summary>
    /// 地区版本接口
    /// </summary>
    public interface IAreaRealize
    {

        //IVMAS VMAS { get; set; }
        /// <summary>
        /// 检测方法
        /// </summary>
        IASM Asm { get; set; }
        //IDIDLE DIDLE { get; set; }
        //ILIGHTPROOF LIGHTPROOF { get; set; }
        //ILUGDOWN LUGDOWN { get; set; } 
        //ILZYD ILZYD { get; set; }
        // void Upload();
        /// <summary>
        /// 打印
        /// </summary>
        void Print();
    }

    /// <summary>
    /// 全国
    /// </summary>
    public class Nationwide : IAreaRealize
    {
        public Nationwide()
        {
            Asm = new ASM_2018A();
        }
        public IASM Asm { get; set; }



        public void Print()
        {

        }

        //public IVMAS VMAS { get; set; }
        //public IDIDLE DIDLE { get; set; }
        //public ILIGHTPROOF LIGHTPROOF { get; set; }
        //public ILUGDOWN LUGDOWN { get; set; }
        //public ILZYD ILZYD { get; set; }
    }


    public class Cq : Nationwide, IAreaRealize
    {
    }
    /// <summary>
    /// 过程控制 不同检测方法 过程是否一致
    /// </summary>
    public class ProcessControl
    {
        //当前过程  
    }


    ////汽油
    //双怠速
    //简易瞬态

    ////柴油
    //自由加速
    //加载减速

    ///// <summary>
    ///// 简易瞬态工况法
    ///// </summary>
    //public interface IVMAS
    //{
    //}

    ///// <summary>
    ///// 双怠速
    ///// </summary>
    //public interface IDIDLE
    //{
    //}

    ///// <summary>
    ///// 自由加速
    ///// </summary>
    //public interface ILIGHTPROOF
    //{
    //}

    ///// <summary>
    ///// 加载减速 限值与判定
    ///// </summary>
    //public interface ILUGDOWN
    //{
    //}

    ///// <summary>
    ///// 滤纸烟度
    ///// </summary>
    //public interface ILZYD
    //{
    //}
}
