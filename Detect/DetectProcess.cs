﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Detect
{
    /// <summary>
    /// 检测过程
    /// </summary>
    public class DetectProcess
    {
        /// <summary>
        /// 开始
        /// </summary>
        public Func<string, bool> StartEvent;
        /// <summary>
        /// 检测中
        /// </summary>
        public Func<string, bool> CentreEvent;
        /// <summary>
        /// 结束
        /// </summary>
        public Func<string, bool> EndEvent;
    }


}
